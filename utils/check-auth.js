export default function isAuth() {
  const localToken = localStorage.getItem("jwtToken");
  return Boolean(localToken);
}
