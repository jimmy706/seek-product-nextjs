import axios from "axios";

function setAuthToken(token) {
  if (token) {
    localStorage.setItem("jwtToken", token);
    axios.defaults.headers.common["Authorization"] = `JWT ${token}`;
  } else {
    localStorage.removeItem("jwtToken", token);
    delete axios.defaults.headers.common["Authorization"];
  }
}

export default setAuthToken;
