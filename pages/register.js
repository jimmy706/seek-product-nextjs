import React, { Component } from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import { Form, Icon, Input, Button, Row, Col } from "antd";
import { connect } from "react-redux";
import { REGISTER_ACTION } from "../constants/sagas-type";

class RegisterPage extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.dispatchRegisterAction(values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Helmet>
          <meta name="description" content="Login" />
          <title>Register</title>
        </Helmet>
        <Layout>
          <div className="wrapper" style={{ padding: "40px 0" }}>
            <Form
              className="register-form"
              style={{ maxWidth: "300px", margin: "auto" }}
              onSubmit={this.handleSubmit}
            >
              <Row gutter={12}>
                <Col span={12}>
                  <Form.Item>
                    {getFieldDecorator("first_name", {
                      rules: [
                        {
                          required: true,
                          message: "Enter your first name!"
                        }
                      ]
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        name="first_name"
                        placeholder="First name..."
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item>
                    {getFieldDecorator("last_name", {
                      rules: [
                        {
                          required: true,
                          message: "Enter your last name!"
                        }
                      ]
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        name="last_name"
                        placeholder="Last name..."
                      />
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item>
                {getFieldDecorator("email", {
                  rules: [
                    { required: true, message: "Please input your email!" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    type="email"
                    name="email"
                    placeholder="Enter your email..."
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("password", {
                  rules: [
                    { required: true, message: "Please input your Password!" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    type="password"
                    name="password"
                    placeholder="Enter your password..."
                    min={4}
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("confirm_password", {
                  rules: [
                    {
                      required: true,
                      message: "Please input your Confirm Password!"
                    }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    type="password"
                    name="confirm_password"
                    placeholder="Enter your confirm password..."
                    min={4}
                  />
                )}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" className="block-btn">
                  Register
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Layout>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchRegisterAction: userInfo =>
      dispatch({ type: REGISTER_ACTION, payload: userInfo })
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Form.create({ name: "register_page" })(RegisterPage));
