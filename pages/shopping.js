import React, {useEffect, useState} from 'react';
import Layout from "../components/Layout";
import isAuth from "../utils/check-auth";
import Router from "next/router";
import Axios from "axios";
import {SEARCH_CATEGORY_URL} from "../constants/APIs";
import {PRODUCT_PAGE_SIZE} from "../constants/global-vars";
import ProductWrapper from "../components/products/ProductWrapper";
import {onDoneAction, onLoadAction} from "../actions/preload.action";
import {connect} from 'react-redux';
import {useRouter} from "next/router";

function ShoppingPage(props){
    const [products, setProducts] = useState(props.products);
    const [nextPage,setNextPage] = useState(props.nextPage);
    const router = useRouter();

    function handleViewMore(nextPage){
        const {category,search} = router.query;
        props.dispatchOnLoadAction();
        Axios.post(nextPage,{category,key_word: search})
            .then(res => {
                setNextPage(res.data.next);
                setProducts(products.concat(res.data.results));
                props.dispatchOnDoneAction();
            })
            .catch(err => {
                console.log(err.response);
            });
    }


    useEffect(()=>{
       if(!isAuth()) {
           Router.push("/login");
       }
    },[]);

    return <div>
        <Layout>
            <ProductWrapper handleViewMore={handleViewMore} nextPage={nextPage} products={products}/>
        </Layout>
    </div>
}

ShoppingPage.getInitialProps = async function({query}){
    const category = query.category ? query.category : '';
    const search = query.search ? query.search : '';

    try {
        const result = await Axios.post(`${SEARCH_CATEGORY_URL}?page_size=${PRODUCT_PAGE_SIZE}`,{category,key_word: search});
        return {
            nextPage: result.data.next,
            products: result.data.results
        }
    }
    catch (e) {
        console.log(e.response);
        return {
            nextPage: null,
            products: []
        }
    }
};

const mapDispatchToProps = dispatch => {
    return {
        dispatchOnLoadAction: () => dispatch(onLoadAction()),
        dispatchOnDoneAction: () => dispatch(onDoneAction()),
    };
};

export default connect(null,mapDispatchToProps)(ShoppingPage);