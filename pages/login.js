import React, { Component } from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import { Form, Icon, Input, Button } from "antd";
import { connect } from "react-redux";
import { LOGIN_ACTION } from "../constants/sagas-type";

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      email: ""
    };
  }

  handleChangeInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.loginAction(this.state.email, this.state.password);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <>
        <Helmet>
          <meta name="description" content="Login" />
          <title>Login</title>
        </Helmet>
        <Layout>
          <div className="wrapper" style={{ padding: "40px 0" }}>
            <Form
              className="login-form"
              style={{ maxWidth: "300px", margin: "auto" }}
              onSubmit={this.handleSubmit}
            >
              <Form.Item>
                {getFieldDecorator("email", {
                  rules: [
                    { required: true, message: "Please input your email!" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    type="email"
                    onChange={this.handleChangeInput}
                    name="email"
                    placeholder="Enter your email..."
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("password", {
                  rules: [
                    { required: true, message: "Please input your Password!" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    onChange={this.handleChangeInput}
                    type="password"
                    name="password"
                    placeholder="Enter your password..."
                    min={4}
                  />
                )}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" className="block-btn">
                  Log in
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Layout>
      </>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loginAction: (email, password) =>
      dispatch({ type: LOGIN_ACTION, payload: { email, password } })
  };
};

const mapStateToProps = state => {
  return {
    preload: state.preload,
    errors: state.errors
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create({ name: "login_page" })(LoginPage));
