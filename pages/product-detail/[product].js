import React from "react";
import {useRouter} from "next/router";
import Layout from "../../components/Layout";
import useSWR from "swr";
import {PREFIX_URL} from "../../constants/global-vars";
import {Helmet} from "react-helmet";
import ProductDetailLoader from "../../components/content-loaders/product-main-info.loader";
import MainDetail from "../../components/product-detail/main-detail/MainDetail";
import Preload from "../../components/preload/Preload";
import ProductDesc from "../../components/product-detail/product-desc/ProductDesc";
import ProductCategory from "../../components/product-detail/product-category/ProductCategory";
import CustomerReview from "../../components/product-detail/customer-review/CustomerReview";
import Faq from "../../components/product-detail/faq/Faq";
import "swiper/css/swiper.min.css";
import RelateProducts from "../../components/product-detail/relate-products/RelateProduct";

const fetcher = url => fetch(url).then(r => r.json());

export default function ProductDetailPage() {
  const router = useRouter();
  const { product, productId } = router.query;
  const { data, error } = useSWR(
    `${PREFIX_URL}/api/products/${productId}/details`,
    fetcher
  );

  function renderContent() {
    if (!data) {
      return <ProductDetailLoader />;
    } else {
      return (
        <div className="product-detail">
          <MainDetail feeds={data.feeds} product={data.product} />
          <ProductDesc
            full_description={data.product.full_description}
            description={data.product.description}
          />
          <Faq faq={data.product.faq}/>
          <RelateProducts relateProducts={data.other_products}/>
          <ProductCategory category={data.product.category} />
          <CustomerReview product={data.product} />
        </div>
      );
    }
  }

  return (
    <div>
      <Helmet>
        <meta name="description" content="Product detail" />
        <title>{product}</title>
      </Helmet>
      <Preload />
      <Layout>{renderContent()}</Layout>
    </div>
  );
}
