import React, {useEffect} from "react";
import {Helmet} from 'react-helmet';
import Router from 'next/router';
import isAuth from "../utils/check-auth";
import {connect} from 'react-redux';
import UserAvatar from "../components/user-profile/avatar-wrapper/UserAvatar";
import Header from "../components/header/Header";
import AccountInfo from "../components/user-profile/account-info/AccountInfo";
import PersonalProfile from "../components/user-profile/personal-profile/PersonalProfile";
import ChangePasswordForm from "../components/user-profile/change-password/ChangePasswordForm";

function ProfilePage(props){
    const { avatar, username, email } = props.userState;
    useEffect(()=>{
        if(!isAuth()){
            Router.push("/login");
        }
    },[]);

    function handleChangeAvatar(e){
        e.preventDefault();
    }

    return (
        <div className='profile-page'>
            <Helmet>
                <meta name="description" content="Your profile" />
                <title>Your profile</title>
            </Helmet>
            <Header/>
            <main>
                <UserAvatar avatar={avatar ? avatar : "https://via.placeholder.com/100x100"} onChange={handleChangeAvatar}/>
                <div className='container'>
                    <AccountInfo username={username} email={email}/>
                    <PersonalProfile currentUser={props.userState}/>
                    <ChangePasswordForm/>
                </div>
            </main>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        userState: state.user
    }
};

export default connect(mapStateToProps,null)(ProfilePage);