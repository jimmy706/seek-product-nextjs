import React,{useEffect,useState} from 'react';
import getCurrency from '../utils/get-currency';
import {useRouter} from "next/router";
import Layout from '../components/Layout';
import Head from "next/head";
import StripeContainer from "../components/stripe-container/StripeContainer";
import isAuth from "../utils/check-auth";
import Preload from "../components/preload/Preload";
function ConfirmPaymentPage(){
    const [stripe,setStripe] = useState(null);
    const router = useRouter();
    const {price} = router.query;
    useEffect(() => {
        if(!isAuth()){
            router.push("/login");
        }

        if(window.Stripe){
            setStripe( window.Stripe(process.env.STRIPE_PUBLISE_KEY));
        }else{
            document.getElementById('stripe-js').addEventListener('load',() => {
                setStripe(window.Stripe(process.env.STRIPE_PUBLISE_KEY));
            })
        }
    },[]);



    return <div className='confirm-payment-page'>
        <Head>
            <script id='stripe-js' src="https://js.stripe.com/v3/" />
            <title>Confirm payment</title>
        </Head>
        <Preload/>
        <div>
            <Layout>
                    <div>
                        <p>Total price: <b>{getCurrency(price)}</b></p>
                        {process.browser ? <StripeContainer stripe={stripe}/> : <div/>}
                </div>
            </Layout>
        </div>

    </div>
}

export default ConfirmPaymentPage;