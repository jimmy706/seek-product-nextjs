import {Provider} from "react-redux";
import App from "next/app";
import withRedux from "next-redux-wrapper";
import makeStore from "../config/makeStore";
import withReduxSaga from "next-redux-saga";
import "antd/dist/antd.min.css";
import "../sass/index.scss";
import {EXPIRED_TOKEN, LOGIN_WITH_TOKEN} from "../constants/sagas-type";
import jwtDecode from "jwt-decode";
import React from "react";
import axios from 'axios';
import {LIST_CATEGORY_URL} from '../constants/APIs';
import firebaseConfig from "../config/firebase-config";



class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};

    return { pageProps };
  }

  async componentDidMount(){
    firebaseConfig();
    const categories = await axios(LIST_CATEGORY_URL);
    localStorage.setItem("categories",JSON.stringify(categories.data.results));

    const {store} = this.props;
    const token = localStorage.getItem("jwtToken");
    if (token) {
      const tokenDecoded = jwtDecode(token);
      if (tokenDecoded.exp < Math.floor(Date.now() / 1000)) {
        store.dispatch({type: EXPIRED_TOKEN});
      } else if (!Object.keys(store.getState().user).length) {
        store.dispatch({type:LOGIN_WITH_TOKEN, payload: {token}})
      }
    }
  }

  render() {
    const { Component, pageProps, store } = this.props;
    return (
          <Provider store={store}>
              <Component {...pageProps} />
          </Provider>
    );
  }
}

export default withRedux(makeStore)(withReduxSaga(MyApp));
