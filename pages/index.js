import axios from "axios";
import { LIST_PRODUCTS_URL } from "../constants/APIs";
import { Helmet } from "react-helmet";
import ProductWrapper from "../components/products/ProductWrapper";
import React, { useState } from "react";
import Layout from "../components/Layout";
import { PRODUCT_PAGE_SIZE } from "../constants/global-vars";
import { connect } from "react-redux";
import { onLoadAction, onDoneAction } from "../actions/preload.action";

function Landing(props) {
  const [products, setProducts] = useState(props.listProduct.results);
  const [nextPage, setNextPage] = useState(props.listProduct.next);
  function handleViewMore(nextPage) {
    props.dispatchOnLoadAction();
    axios(nextPage)
      .then(res => {
        setNextPage(res.data.next);
        setProducts(products.concat(res.data.results));
        props.dispatchOnDoneAction();
      })
      .catch(err => {
        console.log(err.response);
      });
  }

  return (
    <>
      <Helmet>
        <meta name="description" content="Seeking product project" />
        <title>Seek product project</title>
      </Helmet>
      <Layout>
        <ProductWrapper
          handleViewMore={handleViewMore}
          products={products}
          nextPage={nextPage}
        />
      </Layout>
    </>
  );
}

Landing.getInitialProps = async function() {
  const result = await axios(
    `${LIST_PRODUCTS_URL}?page_size=${PRODUCT_PAGE_SIZE}`
  );
  return {
    listProduct: result.data
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatchOnLoadAction: () => dispatch(onLoadAction()),
    dispatchOnDoneAction: () => dispatch(onDoneAction()),
  };
};

const mapStateToProps = state => {
  return {
    userState: state.user
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
