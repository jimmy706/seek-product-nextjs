import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import CartItem from "../components/cart-item/CartItem";
import PaymentBoard from "../components/cart-item/PaymentBoard";
import Preload from "../components/preload/Preload";
import Router from "next/router";
import isAuth from "../utils/check-auth";
import EmptyCart from "../components/cart-item/EmptyCart";

function CartPage(props) {
  useEffect(() => {
    if (!isAuth()) {
      Router.push("/login");
    }
  }, []);

  function renderCartItems() {
    return props.cartItems.map(item => (
      <CartItem key={item.product.id} item={item} />
    ));
  }

  return (
    <div className="cart-page">
      <Preload />
      <Helmet>
        <meta name="description" content="Cart items" />
        <title>Your cart</title>
      </Helmet>
      <Layout>
        <div className="cart">
          {props.cartItems.length ? (
            <>
              <div className="cart__items">{renderCartItems()}</div>
              <PaymentBoard />
            </>
          ) : (
            <EmptyCart />
          )}
        </div>
      </Layout>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    cartItems: state.cart.items
  };
};

export default connect(mapStateToProps, null)(CartPage);
