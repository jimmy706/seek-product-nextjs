import * as firebase from "firebase/app";
import "firebase/analytics";
import "firebase/messaging";

function firebaseConfig() {
    const firebaseConfig = {
        apiKey: "AIzaSyBqjUy7dUqgrhTXZCv_V9XGzj3Ts2PV9Vg",
        authDomain: "seek-product-nextjs-a7852.firebaseapp.com",
        databaseURL: "https://seek-product-nextjs-a7852.firebaseio.com",
        projectId: "seek-product-nextjs-a7852",
        storageBucket: "seek-product-nextjs-a7852.appspot.com",
        messagingSenderId: "590727616347",
        appId: "1:590727616347:web:10f17b38b2bc3edb8a2c7e",
        measurementId: "G-7H83FNS61C"
    };
    if (!firebase.apps.length) {
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        firebase.analytics();

        // Create firebase messaging
        const messaging = firebase.messaging();
        messaging.usePublicVapidKey('BIEm1vNv0nID4_187pAurXBOJ-6fbyYX-J8o7asryHS6Y8G-U05e5v67d5eBuJPNgpGwrOp7Ga3VsM7_zfEFgCE');
        messaging.getToken().then((currentToken) => {
            console.log('Current token: ', currentToken);
            localStorage.setItem('messageToken', currentToken);
        }).catch(err => {
            console.log(err);
        });
        messaging.onTokenRefresh(()=>{
            messaging.getToken().then((refreshToken) => {
                console.log('Refresh token: ', refreshToken);
                localStorage.setItem('messageToken', refreshToken);
            }).catch(err => {
                console.log(err);
            });
        });
        messaging.onMessage((message)=>{
            const {notification} = message;
            console.log(notification);
        });
    }

}

export default firebaseConfig;