import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers/root.reducer";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas/root.saga";

const makeStore = (initialState, { req = null, isServer }) => {
  const sagaMiddleWare = createSagaMiddleware();
  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(sagaMiddleWare))
  );
  if (req || !isServer) {
    store.sagaTask = sagaMiddleWare.run(rootSaga);
  }

  return store;
};

export default makeStore;
