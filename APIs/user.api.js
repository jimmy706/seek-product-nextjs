import {
  LOGIN_URL,
  GET_PROFILE_URL,
  REGISTER_URL,
  LOGOUT_URL,
  UPDATE_PROFILE_URL, UPDATE_PASSWORD_URL
} from "../constants/APIs";
import axios from "axios";

export function login(email, password) {
  return axios.post(LOGIN_URL, { email, password });
}

export function getUserProfile() {
  return axios(GET_PROFILE_URL);
}

export function register(userInfo) {
  return axios.post(REGISTER_URL, userInfo);
}

export function logout() {
  return axios.get(LOGOUT_URL);
}

export function updateProfile(userProfile){
  const formData = new FormData();
  for(let key in userProfile){
    formData.append(key,userProfile[key]);
  }
  return axios.put(UPDATE_PROFILE_URL,userProfile);
}

export function updatePassword(passwords) {
  return axios.put(UPDATE_PASSWORD_URL,passwords);
}
