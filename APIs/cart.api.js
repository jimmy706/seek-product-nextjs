import {
  VIEW_CART_URL,
  UPDATE_CART_URL,
  ADD_TO_CART_URL,
  REMOVE_FROM_CART_URL, CART_PAYMENT_URL
} from "../constants/APIs";
import axios from "axios";

export function viewCart() {
  return axios.get(VIEW_CART_URL);
}

export function updateCart(product, amount) {
  return axios.post(UPDATE_CART_URL, { product, amount });
}

export function addToCart(product, amount) {
  return axios.post(ADD_TO_CART_URL, { product, amount });
}

export function removeFromCart(product) {
  return axios.post(REMOVE_FROM_CART_URL, { product });
}

export function payment(token) {
  return axios.post(CART_PAYMENT_URL,{token,option: 2});
}