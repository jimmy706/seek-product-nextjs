import { SET_ERROR } from "../constants/action-type";
import { message } from "antd";

export function setErrorAction(errors) {
  for (let key in errors) {
    message.error(errors[key][0]);
  }
  return {
    type: SET_ERROR,
    errors
  };
}
