import { ON_DONE, ON_LOAD } from "../constants/action-type";

export function onLoadAction() {
  return {
    type: ON_LOAD
  };
}

export function onDoneAction() {
  return {
    type: ON_DONE
  };
}
