import {CHANGE_SEARCH_PRODUCT} from "../constants/action-type";

export function changeSearchValue(val){
    return {
        type: CHANGE_SEARCH_PRODUCT,
        query: val
    }
}