import { UPDATE_CART } from "../constants/action-type";

export function updateCart(cartObj) {
  const { cart_detail, total } = cartObj;
  let size = 0;
  for (let item of cart_detail) {
    size += item.amount;
  }

  return {
    type: UPDATE_CART,
    payload: {
      size,
      total,
      items: cart_detail
    }
  };
}

export function emptyCart() {
  return {
    type: UPDATE_CART,
    payload: {
      size: 0,
      total: 0,
      items: []
    }
  };
}
