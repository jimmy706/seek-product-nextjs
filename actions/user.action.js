import { SET_CURRENT_USER, LOGOUT } from "../constants/action-type";

export function setCurrentUserAction(currentUser) {
  const {
    email,
    username,
    first_name,
    last_name,
    phone_number,
    work_at,
    job,
    message,
    avatar,
    show_phone
  } = currentUser;
  return {
    type: SET_CURRENT_USER,
    payload: {
      email,
      username,
      first_name,
      last_name,
      phone_number,
      work_at,
      job,
      message,
      avatar,
      show_phone
    }
  };
}

export function logoutAction() {
  return {
    type: LOGOUT
  };
}
