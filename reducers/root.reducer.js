import { combineReducers } from "redux";
import preloadReducer from "./preload.reducer";
import userReducer from "./user.reducer";
import errorsReducer from "./errors.reducers";
import cartReducer from "./cart.reducer";
import queryProductReducer from "./queryProduct.reducer";

const rootReducer = combineReducers({
  preload: preloadReducer,
  user: userReducer,
  errors: errorsReducer,
  cart: cartReducer,
  queryProduct: queryProductReducer
});

export default rootReducer;
