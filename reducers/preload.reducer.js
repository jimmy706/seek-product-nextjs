import { ON_LOAD, ON_DONE } from "../constants/action-type";

const initialState = false;

function preloadReducer(state = initialState, action) {
  switch (action.type) {
    case ON_LOAD:
      return true;
    case ON_DONE:
      return false;
    default:
      return state;
  }
}

export default preloadReducer;
