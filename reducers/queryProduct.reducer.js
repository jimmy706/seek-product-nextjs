import {CHANGE_SEARCH_PRODUCT} from "../constants/action-type";

const initialState = '';

function queryProductReducer(state = initialState, action){
    switch (action.type) {
        case CHANGE_SEARCH_PRODUCT:
            return action.query;
        default:
            return state;
    }
}

export default queryProductReducer;