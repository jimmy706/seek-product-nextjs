import { SET_ERROR } from "../constants/action-type";

const initialState = {};

function errorsReducer(state = initialState, action) {
  switch (action.type) {
    case SET_ERROR:
      return action.errors;
    default:
      return { ...state };
  }
}

export default errorsReducer;
