import { UPDATE_CART } from "../constants/action-type";

const initialState = {
  size: 0,
  items: [],
  total: 0
};

function cartReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_CART:
      return {
        size: action.payload.size,
        items: [...action.payload.items],
        total: action.payload.total
      };
    default:
      return { ...state };
  }
}

export default cartReducer;
