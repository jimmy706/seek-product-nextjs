import { SET_CURRENT_USER, LOGOUT } from "../constants/action-type";

const initialState = {};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return action.payload;
    case LOGOUT:
      return {};
    default:
      return { ...state };
  }
}
