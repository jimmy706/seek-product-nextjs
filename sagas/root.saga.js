import {all} from "redux-saga/effects";
import {
  login,
  loginWithCurrentToken,
  register,
  updatePassword,
  updateUserProfile,
  watchExpiredToken,
  watchLogout
} from "./user.saga";

import {getCartInfo, handleAddToCart, handlePayment, handleRemoveFromCart, handleUpdateCart} from "./cart.saga";

export default function* rootSaga() {
  yield all([
    login(),
    loginWithCurrentToken(),
    register(),
    watchExpiredToken(),
    watchLogout(),
    getCartInfo(),
    handleAddToCart(),
    handleRemoveFromCart(),
    handleUpdateCart(),
    updateUserProfile(),
    updatePassword(),
    handlePayment()
  ]);
}
