import {call, put, take, takeLatest} from "redux-saga/effects";
import {
  ADD_TO_CART,
  CONFIRM_PAYMENT_SAGA,
  GET_CART_INFO,
  REMOVE_ITEM_FROM_CART,
  UPDATE_CART_ITEM
} from "../constants/sagas-type";
import * as cartAPIs from "../APIs/cart.api";
import {emptyCart, updateCart} from "../actions/cart.action";
import {onDoneAction, onLoadAction} from "../actions/preload.action";
import {message} from 'antd';
import Router from "next/router";


export function* getCartInfo() {
  yield takeLatest(GET_CART_INFO, function*() {
    try {
      const cartInfo = yield call(cartAPIs.viewCart);
      yield put(updateCart(cartInfo.data));
    } catch (err) {
      console.log(err);
    }
  });
}

export function* handleAddToCart() {
  while (true) {
    const { product, amount } = yield take(ADD_TO_CART);
    yield put(onLoadAction());
    try {
      const result = yield call(cartAPIs.addToCart, product, amount);
      yield put(updateCart(result.data));
    } catch (err) {
      console.log(err.response);
    } finally {
      yield put(onDoneAction());
    }
  }
}

export function* handleUpdateCart() {
  while(true){
    const {product, amount} = yield take(UPDATE_CART_ITEM);
    yield  put(onLoadAction());
    try {
      const result = yield call(cartAPIs.updateCart,product,amount);
      yield put(updateCart(result.data));
    }
    catch (e) {
      message.error(e.response.data.message);
    }
    finally {
      yield put(onDoneAction());
    }
  }
}

export function* handleRemoveFromCart() {
  while (true) {
    const { product } = yield take(REMOVE_ITEM_FROM_CART);
    yield put(onLoadAction());
    try {
      const result = yield call(cartAPIs.removeFromCart, product);
      yield put(updateCart(result.data));
    } catch (err) {
      console.log(err.response);
    } finally {
      yield put(onDoneAction());
    }
  }
}

export function *handlePayment() {
  while(true) {
    const action = yield take(CONFIRM_PAYMENT_SAGA);
    const {token} = action.payload;
    yield put(onLoadAction());
    try {
      const result = yield call(cartAPIs.payment,token);
     if(result.data.message === 'SUCCESSFUL!!!'){
       yield put(emptyCart());
       message.success("Payment success!!");
       Router.push("/");
     }
    }
    catch (e) {
      console.log(e);
    }
    finally {
      yield put(onDoneAction());
    }
  }
}
