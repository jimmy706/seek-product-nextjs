import { take, call, put, takeLatest, fork } from "redux-saga/effects";
import * as userAPI from "../APIs/user.api";
import {
  LOGIN_ACTION,
  REGISTER_ACTION,
  LOGOUT_ACTION,
  EXPIRED_TOKEN,
  LOGIN_WITH_TOKEN, UPDATE_PROFILE, UPDATE_PASSWORD
} from "../constants/sagas-type";
import setAuth from "../utils/set-auth";
import { setCurrentUserAction, logoutAction } from "../actions/user.action";
import { setErrorAction } from "../actions/errors.action";
import { onDoneAction, onLoadAction } from "../actions/preload.action";
import { emptyCart } from "../actions/cart.action";
import Router from "next/router";
import { message as alertMessage } from "antd";
import jwtDecode from "jwt-decode";

function handleExpiredToken(timeout) {
  setTimeout(function*() {
    setAuth("");
    yield put(logoutAction());
  }, timeout);
}

export function* login() {
  while (true) {
    const action = yield take(LOGIN_ACTION);
    yield put(onLoadAction());
    const { email, password } = action.payload;
    try {
      const loginResult = yield call(userAPI.login, email, password);
      const { token } = loginResult.data;
      setAuth(token);

      const expiredTime = jwtDecode(token).exp * 1000;
      yield fork(handleExpiredToken, expiredTime - Date.now());

      const userProfile = yield call(userAPI.getUserProfile);
      yield put(setCurrentUserAction(userProfile.data));
      yield put(setErrorAction({}));
      Router.push("/");
      alertMessage.success("Login success!!");
    } catch (err) {
      yield put(setErrorAction(err.response.data));
    } finally {
      yield put(onDoneAction());
    }
  }
}

export function* register() {
  while (true) {
    const action = yield take(REGISTER_ACTION);
    yield put(onLoadAction());
    try {
      const registerResult = yield call(userAPI.register, action.payload);
      yield put(setErrorAction({}));
      Router.push("/login");
      alertMessage.success("Create account success!!");
    } catch (err) {
      yield put(setErrorAction(err.response.data));
      console.log(err.response);
    } finally {
      yield put(onDoneAction());
    }
  }
}

export function* loginWithCurrentToken() {
  const action = yield take(LOGIN_WITH_TOKEN);
  const { token } = action.payload;
  setAuth(token);
  try {
    const userProfile = yield call(userAPI.getUserProfile);
    yield put(setCurrentUserAction(userProfile.data));
  } catch (err) {
    console.log(err.response);
  }
}

export function* watchExpiredToken() {
  yield takeLatest(EXPIRED_TOKEN, function*() {
    setAuth("");
    yield put(logoutAction());
  });
}

export function* watchLogout() {
  yield takeLatest(LOGOUT_ACTION, function*() {
    try {
      yield call(userAPI.logout);
      setAuth("");
      yield put(logoutAction());
      yield put(emptyCart());
      alertMessage.success("Logout success!");
      Router.push("/");
    } catch (err) {
      console.log(err.response);
    }
  });
}

export function* updateUserProfile(){
  while(true){
    const action = yield take(UPDATE_PROFILE);
    const userProfile = action.payload;
    yield put(onLoadAction());

    try{
      const result = yield call(userAPI.updateProfile,userProfile);
      if(result.data.message === "PROFILE_UPDATED"){
        alertMessage.success("Profile updated");
        yield put(setCurrentUserAction(userProfile));
      }
      console.log(result);
    }
    catch (e) {
      console.log(e);
    }
    finally {
      yield put(onDoneAction());
    }
  }
}

export function* updatePassword(){
  while(true){
    const action = yield take(UPDATE_PASSWORD);
    yield put(onLoadAction());
    try{
      const result = yield call(userAPI.updatePassword,action.payload);
      if(result.data.message === 'PASSWORD_UPDATED'){
        alertMessage.success("Password updated");
      }
      yield put(logoutAction());
      Router.push("/login");
    }
    catch (e) {
      yield put(setErrorAction(e.response.data));
    }
    finally {
      yield put(onDoneAction());
    }
  }
}