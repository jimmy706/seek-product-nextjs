importScripts('https://www.gstatic.com/firebasejs/7.8.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.8.0/firebase-messaging.js');


firebase.initializeApp({
    apiKey: "AIzaSyBqjUy7dUqgrhTXZCv_V9XGzj3Ts2PV9Vg",
    authDomain: "seek-product-nextjs-a7852.firebaseapp.com",
    databaseURL: "https://seek-product-nextjs-a7852.firebaseio.com",
    projectId: "seek-product-nextjs-a7852",
    storageBucket: "seek-product-nextjs-a7852.appspot.com",
    messagingSenderId: "590727616347",
    appId: "1:590727616347:web:10f17b38b2bc3edb8a2c7e",
    measurementId: "G-7H83FNS61C"
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});

