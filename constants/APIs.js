import { PREFIX_URL } from "./global-vars";

export const GET_PROFILE_URL = PREFIX_URL + "/api/auth/profile";
export const LOGIN_URL = PREFIX_URL + "/user/login/";
export const REGISTER_URL = PREFIX_URL + "/api/auth/register/";
export const LOGOUT_URL = PREFIX_URL + "/user/logout/";
export const UPDATE_PROFILE_URL = PREFIX_URL + "/api/auth/profile";
export const UPDATE_PASSWORD_URL = PREFIX_URL + "/api/auth/change_pass";

export const SEARCH_CATEGORY_URL = PREFIX_URL + "/api/category/search/";
export const LIST_CATEGORY_URL = PREFIX_URL + "/api/category";
export const LIST_PRODUCTS_URL = PREFIX_URL + "/api/products/list";

export const VIEW_CART_URL = PREFIX_URL + "/api/user/cart/view-cart/";
export const UPDATE_CART_URL = PREFIX_URL + "/api/user/cart/update-cart";
export const REMOVE_FROM_CART_URL =
  PREFIX_URL + "/api/user/cart/remove-from-cart";
export const ADD_TO_CART_URL = PREFIX_URL + "/api/user/cart/add-to-cart";
export const CART_PAYMENT_URL = PREFIX_URL + "/api/user/cart/payment";

export const CREATE_REVIEW_URL = PREFIX_URL + "/api/user/review/create";
