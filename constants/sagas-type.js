export const GET_CART_INFO = "GET_CART_INFO";

export const LOGIN_ACTION = "LOGIN_ACTION";
export const LOGOUT_ACTION = "LOGOUT_ACTION";
export const REGISTER_ACTION = "REGISTER_ACTION";
export const LOGIN_WITH_TOKEN = "LOGIN_WITH_TOKEN";
export const EXPIRED_TOKEN = "EXPIRED_TOKEN";
export const UPDATE_PROFILE = "UPDATE_PROFILE";
export const UPDATE_PASSWORD = "UPDATE_PASSWORD";

export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_ITEM_FROM_CART = "REMOVE_ITEM_FROM_CART";
export const UPDATE_CART_ITEM = 'UPDATE_CART_ITEM';
export const CONFIRM_PAYMENT_SAGA = 'CONFIRM_PAYMENT_SAGA';