export const ON_LOAD = "ON_LOADED";
export const ON_DONE = "ON_DONE";

export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const LOGOUT = "LOGOUT";

export const SET_ERROR = "SET_ERROR";

export const UPDATE_CART = "UPDATE_CART";

export const CHANGE_SEARCH_PRODUCT = 'CHANGE_SEARCH_PRODUCT';