import React from "react";
import Header from "./header/Header";
function Layout(props) {

  return (
    <div>
      <Header/>

      <main>


        <div className="container">{props.children}</div>
      </main>
    </div>
  );
}

export default Layout;
