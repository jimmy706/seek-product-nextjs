import React from "react";
import ProductBlock from "./ProductBlock";
import { Button } from "antd";
import PropTypes from "prop-types";
import { connect } from "react-redux";

function ProductWrapper(props) {
  function renderProduct() {
    const { products } = props;
    return products.map(product => (
      <ProductBlock product={product} key={product.id} />
    ));
  }

  function handleViewMore() {
    props.handleViewMore(props.nextPage);
  }

  return (
    <div className="product-wrapper wrapper">
      <div className="products">{renderProduct()}</div>
      {props.nextPage ? (
        <Button
          loading={props.preload}
          className="viewmore-btn"
          onClick={handleViewMore}
        >
          View more...
        </Button>
      ) : null}
    </div>
  );
}

ProductWrapper.propTypes = {
  products: PropTypes.array,
  nextPage: PropTypes.string,
  handleViewMore: PropTypes.func
};

const mapStateToProps = state => {
  return {
    preload: state.preload
  };
};

export default connect(mapStateToProps, null)(ProductWrapper);
