import React from "react";
import { Card } from "antd";
import Link from "next/link";
import getCurrency from "../../utils/get-currency";
import { Rate } from "antd";
import PropTypes from 'prop-types';

export default function ProductBlock(props) {
  const { name, price, id, image, rating } = props.product;

  return (
    <Link
      href={`/product-detail/[product]?productId=${id}`}
      as={`/product-detail/${name}?productId=${id}`}
    >
      <a className="product">
        <Card
          bordered={false}
          cover={
            <img
              alt="product display"
              src={image ? image : "https://via.placeholder.com/400x400"}
              className="product__img"
            />
          }
        >
          <div className="product__content">
            <p className="product__name">{name}</p>
            <h4 className="product__price">{getCurrency(price)}</h4>
            <Rate style={{ fontSize: "14px" }} disabled defaultValue={rating} />
          </div>
        </Card>
      </a>
    </Link>
  );
}

ProductBlock.propTypes = {
  product: PropTypes.object.isRequired
};
