import React from 'react';
import {CardNumberElement,
    CardExpiryElement,
    CardCVCElement, injectStripe} from "react-stripe-elements";
import {Button} from "antd";
import {connect} from 'react-redux';
import {CONFIRM_PAYMENT_SAGA} from "../../constants/sagas-type";



function CheckoutForm(props){

    async function submit(e) {
        e.preventDefault();
        const {first_name,last_name} = props.userState;
        try {
            // TODO: Create a token to securely transmit card information
            const element = {
                name: `${first_name} ${last_name}`
            };

            let { token } = await props.stripe.createToken(element);
            console.log(token);
            props.dispatchConfirmPayment(token.id);
        } catch (err) {
            console.log(err.response);
        }
    }

    return (
        <div className="checkout-form">
            <form>
                <div className="form-group">
                    <label>Card number</label>
                    <CardNumberElement className="form-control" />
                </div>
                <div className="form-group">
                    <label>Expiration date</label>
                    <CardExpiryElement />
                </div>
                <div className="form-group">
                    <label>CVC</label>
                    <CardCVCElement />
                </div>
                <Button type={'primary'} htmlType={'submit'} onClick={submit}>Purchase</Button>
            </form>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        userState: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        dispatchConfirmPayment: (token) => dispatch({type: CONFIRM_PAYMENT_SAGA, payload: {token}})
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(injectStripe(CheckoutForm));