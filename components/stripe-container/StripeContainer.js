import React from 'react';
import { StripeProvider, Elements } from 'react-stripe-elements'
import CheckoutForm from "../checkout-form/CheckoutForm";


export default function StripeContainer(props){
    return (
        <StripeProvider stripe={props.stripe}>
            <Elements>
                <CheckoutForm/>
            </Elements>
        </StripeProvider>
    )
}