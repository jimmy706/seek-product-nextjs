import React from "react";
import Link from "next/link";
import PropTypes from "prop-types";

export default function ProductCategory(props) {
  function renderCategories() {
    const { category } = props;
    return category.map(cate => (
      <li className="cate" key={cate.id}>
        <Link href={`/shopping?category=${cate.name}`}>
          <a>{cate.name}</a>
        </Link>
      </li>
    ));
  }

  return (
    <div className="product-categories product-section">
      <div>
        <h3>Categories</h3>
        <hr />
        <ul className="category-wrapper">{renderCategories()}</ul>
      </div>
    </div>
  );
}

ProductCategory.propTypes = {
  category: PropTypes.array
};
