import React, {useEffect} from "react";
import Swiper from "swiper/js/swiper.min";
import PropTypes from 'prop-types';
import ProductBlock from "../../products/ProductBlock";


export default function RelateProducts(props){
    useEffect(()=>{
        new Swiper(".swiper-container", {
            slidesPerView: 4,
            spaceBetween: 30,
            slidesPerGroup: 4,
            loopFillGroupWithBlank: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev"
            }
        });
    },[]);

    function renderSwiperSlide(){
        return props.relateProducts.map(item => {
            return (
                <div key={item.id} className="swiper-slide">
                    <ProductBlock product={item} />
                </div>
            );
        });
    }

    return (
        <div className="relate-products product-section">
            <h3>Relate products:</h3>
            <hr />
            <div className="swiper-container">
                <div className="swiper-wrapper">{renderSwiperSlide()}</div>
                <div className="swiper-pagination"></div>
                <div className="swiper-button-next"></div>
                <div className="swiper-button-prev"></div>
            </div>
        </div>
    )
}

RelateProducts.propTypes = {
    relateProducts: PropTypes.array.isRequired
};