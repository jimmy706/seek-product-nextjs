import React, {Component} from "react";
import {Button, Input, Rate} from "antd";
import {connect} from 'react-redux';
import axios from 'axios';
import {CREATE_REVIEW_URL} from "../../../constants/APIs";

class YourReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 0,
      content: ""
    };
  }

  handleChangeRating = value => {
    this.setState({ rating: value });
  };

  handleChangeReviewContent = e => {
    const value = e.target.value;
    this.setState({ content: value });
  };

  handleSubmitReview = e => {
    e.preventDefault();
    const { rating, content } = this.state;
    const { productId } = this.props;
    const { first_name, last_name, email } = this.props.currentUser;
    const formData = new FormData();
    formData.append("name", `${first_name} ${last_name}`);
    formData.append("email", email);
    formData.append("rating", rating);
    formData.append("content", content);
    formData.append("product", productId);
    axios.post(CREATE_REVIEW_URL,formData)
        .then(res => {
          this.setState({ content: "", rating: 0 });
          this.props.addReview(res.data);
        })
        .catch(err => {
          console.log(err);
        });
  };

  render() {
    const { TextArea } = Input;
    const { rating, content } = this.state;
    return (
      <form
        className="your-review"
        onSubmit={this.handleSubmitReview}
        style={{ maxWidth: "700px" }}
      >
        <ol>
          <li>
            Your rating: <br />
            <Rate value={rating} onChange={this.handleChangeRating} />
          </li>
          <li>
            Your review: <br />
            <TextArea
              label="Write your review"
              onChange={this.handleChangeReviewContent}
              value={content}
              placeholder="Write your review here..."
            />
          </li>
        </ol>
        <div className="form-actions">
          <Button type="primary" htmlType="submit">
            Send review
          </Button>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.user
  }
}

export default connect(mapStateToProps, null)(YourReview);