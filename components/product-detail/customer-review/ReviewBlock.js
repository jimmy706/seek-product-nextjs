import React from "react";
import { Rate } from "antd";
import moment from "moment";
import PropTypes from "prop-types";

export default function ReviewBlock(props) {
  const { user_details, name, created_at, content, rating } = props.review;
  return (
    <li className="review-block">
      <div className="review-header">
        <img
          src={
            user_details
              ? user_details.avatar
                ? user_details.avatar
                : "https://via.placeholder.com/150x150"
              : "https://via.placeholder.com/150x150"
          }
          alt="user avatar"
          className="user-avatar"
        />
        <span>
          By <b className="display-name">{name}</b>
        </span>
      </div>
      <div className="review-info">
        <Rate defaultValue={rating} style={{ fontSize: "14px" }} />
        <p className="review-time">
          {moment(created_at, "YYYYMMDD").fromNow()}
        </p>
      </div>
      <div className="review-content">
        <p>{content}</p>
      </div>
    </li>
  );
}

ReviewBlock.propTypes = {
  review: PropTypes.object
};
