import React, {useState} from "react";
import PropTypes from "prop-types";
import ReviewBlock from "./ReviewBlock";
import {Collapse} from "antd";
import YourReview from "./YourReview";
import isAuth from "../../../utils/check-auth";
import Link from "next/link";

export default function CustomerReview(props) {
  const { Panel } = Collapse;
  const [reviews, setReviews] = useState(props.product.reviews);
  function renderReviews() {
    return reviews.map(review => {
      return <ReviewBlock review={review} key={review.id} />;
    });
  }

  function addReview(newReview) {
    setReviews(reviews.concat(newReview));
  }

  return (
    <div className="customer-review product-section">
      <h3>Customer reviews:</h3>
      <hr />
      <Collapse defaultActiveKey={["1"]}>
        <Panel header="Write review" key="1">
          {isAuth() ? (
            <YourReview addReview={addReview} productId={props.product.id}/>
          ) : (
            <p>
              You must{" "}
              <Link href="/login">
                <a>Login</a>
              </Link>{" "}
              to use this feature
            </p>
          )}
        </Panel>
      </Collapse>
      <ul className="review-list">{renderReviews()}</ul>
    </div>
  );
}

CustomerReview.propTypes = {
  product: PropTypes.object.isRequired
};
