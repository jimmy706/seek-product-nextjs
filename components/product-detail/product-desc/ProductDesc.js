import React, { useState } from "react";
import { Button } from "antd";
import PropTypes from "prop-types";

export default function ProductDesc(props) {
  const { full_description, description } = props;
  const [viewFull, setViewFull] = useState(false);

  return (
    <div className="product-desc product-section">
      <h3>Product description:</h3>
      <hr />
      {viewFull ? (
        <div dangerouslySetInnerHTML={{ __html: full_description }} />
      ) : (
        <div dangerouslySetInnerHTML={{ __html: description }} />
      )}
      <Button className="viewmore-btn" onClick={() => setViewFull(!viewFull)}>
        {viewFull ? "Collapse" : "View more"}
      </Button>
    </div>
  );
}

ProductDesc.propTypes = {
  full_description: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};
