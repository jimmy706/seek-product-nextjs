import ProductGallery from "./ProductGallery";
import ProductInfo from "./ProductInfo";
import PropTypes from "prop-types";

function MainDetail(props) {
  const { feeds, product } = props;

  return (
    <div className="product-main-info product-section">
      <ProductGallery feeds={feeds} />
      <ProductInfo product={product} />
    </div>
  );
}

MainDetail.propTypes = {
  feeds: PropTypes.array,
  product: PropTypes.object
};

export default MainDetail;
