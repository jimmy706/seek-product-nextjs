import React, { useState } from "react";
import PropTypes from "prop-types";
export default function ProductGallery(props) {
  const { feeds } = props;
  const [displayImg, setDisplayImg] = useState(0);
  return (
    <div className="product-detail__gallery">
      <img
        className="main-img"
        src={feeds[displayImg].url}
        alt={feeds[displayImg].name}
      />
    </div>
  );
}

ProductGallery.propTypes = {
  feeds: PropTypes.array
};
