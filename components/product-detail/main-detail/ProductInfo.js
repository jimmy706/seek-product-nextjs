import React, { useState } from "react";
import PropTypes from "prop-types";
import getCurrency from "../../../utils/get-currency";
import { Button, Icon } from "antd";
import QuantityInput from "../../QuantityInput/QuantityInput";
import isAuth from "../../../utils/check-auth";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import { ADD_TO_CART } from "../../../constants/sagas-type";
import { Rate } from "antd";

function ProductInfo(props) {
  const router = useRouter();
  const { name, rating, model, price, in_stock, vat, id } = props.product;
  const [amount, setAmount] = useState(1);
  function handleAddToCart() {
    if (!isAuth()) {
      router.push("/login");
    } else {
      props.dispatchAddToCart(id, amount);
    }
  }

  function handleChangeQuantity(value) {
    setAmount(value);
  }

  return (
    <div className="product-detail__main-info">
      <div className="detail-header">
        <h3 className="product-name">{name}</h3>
        <Rate disabled defaultValue={rating} style={{ fontSize: "14px" }} />
        <p>Model: {model}</p>
        <hr />
      </div>
      <div className="detail-price">
        <ul>
          <li>
            <h2>{getCurrency(price)}</h2>
          </li>
          <li>In stock: {getCurrency(in_stock)}</li>
          <li>Vat: {getCurrency(vat)}</li>
        </ul>
        <hr />
      </div>
      <div className="product-action">
        <ul>
          <li>
            <QuantityInput onChange={handleChangeQuantity} />
          </li>
          <li className="add-cart-action">
            <Button type="danger" size="large" onClick={handleAddToCart}>
              <Icon style={{ fontSize: "1.1rem" }} type="shopping-cart" />
              Add to cart
            </Button>
          </li>
        </ul>
      </div>
    </div>
  );
}

ProductInfo.propTypes = {
  product: PropTypes.object
};

const mapDispatchToProps = dispatch => {
  return {
    dispatchAddToCart: (product, amount) =>
      dispatch({ type: ADD_TO_CART, product, amount })
  };
};

export default connect(null, mapDispatchToProps)(ProductInfo);
