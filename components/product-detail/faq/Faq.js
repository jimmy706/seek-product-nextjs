import React from 'react';
import PropTypes from 'prop-types';
import {Collapse} from "antd";

function Faq(props){
    const { Panel } = Collapse;
    return <div className='product-faq product-section'>
        <h3>Product FAQs:</h3>
        <hr />
        <Collapse>
            <Panel key="2" header="View FAQ">
                <div dangerouslySetInnerHTML={{ __html: props.faq }} />
            </Panel>
        </Collapse>
    </div>
}

export default Faq;

Faq.propTypes = {
    faq: PropTypes.string.isRequired
}