import Link from "next/link";
import HeaderInteract from "./HeaderInteract";
import React from 'react';
import { Dropdown, Icon} from 'antd';

import Categories from "./Categories";
import SearchHeader from "./SearchHeader";


function Header(props) {


  return (
    <header className="header">
      <ul className="header__nav">
        <li className="header__li">
          <Link href="/">
            <a className="link">Home</a>
          </Link>
        </li>
          <li className='header__li'>
              <Dropdown overlay={Categories}>
                  <a href="#">Categories <Icon type="down" /></a>
              </Dropdown>
          </li>
          <li>
             <SearchHeader/>
          </li>
      </ul>
      <HeaderInteract />
    </header>
  );
}



export default (Header);
