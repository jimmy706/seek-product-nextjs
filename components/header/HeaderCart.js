import React, { useEffect } from "react";
import { Icon } from "antd";
import Link from "next/link";
import { connect } from "react-redux";
import { GET_CART_INFO } from "../../constants/sagas-type";

function HeaderCart(props) {
  useEffect(() => {
    props.dispatchViewCart();
  }, []);

  return (
    <Link href="/cart">
      <a className="header__cart" title="View your cart">
        <Icon type="shopping-cart" />
        <span className="cart-badges">{props.cartSize}</span>
      </a>
    </Link>
  );
}

const mapStateToProps = state => {
  return {
    cartSize: state.cart.size
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatchViewCart: () => {
      dispatch({ type: GET_CART_INFO });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderCart);
