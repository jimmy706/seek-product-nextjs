import {connect} from "react-redux";
import React from "react";

function CategoryItem(props){
    const {category} = props;
    return (
        <li className='category-item' >
            <a href={`/shopping?category=${category.name}&search=${props.search}`}>{category.name}</a>
        </li>
    )
}

const mapStateToProps = state => {
    return {
        search: state.queryProduct
    }
};
export default connect(mapStateToProps,null)(CategoryItem);