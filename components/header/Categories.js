import React from "react";
import {Menu} from 'antd';
import CategoryItem from "./CategoryItem";


function Categories(){

    function renderCategories(){
        const categories = JSON.parse(localStorage.getItem('categories'));
        return categories.map(cate => <CategoryItem key={cate.id} category={cate}/>)
    }
    return <Menu>
        <ul className='category-list'>{renderCategories()}</ul>
    </Menu>
}

export default Categories;