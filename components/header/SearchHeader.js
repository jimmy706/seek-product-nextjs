import React from "react";
import {Input} from 'antd';
import {changeSearchValue} from "../../actions/queryProduct.action";
import {connect} from 'react-redux';
const {Search} = Input;

function SearchHeader(props){
    function handleSearch(value){

        if(window){
            window.location.href = `/shopping?search=${value}&category=`;
        }
    }

    function handleChangeSearch(e){
        const value = e.target.value.trim();
        props.dispatchChangeSearchValue(value);
    }
    return <Search
        placeholder="Search product..."
        allowClear={true}
        onChange={handleChangeSearch}
        onSearch={handleSearch}
        required={true}
    />
}


const mapDispatchToProps = dispatch => {
    return {
        dispatchChangeSearchValue: (value) => dispatch(changeSearchValue(value))
    }
};
export default connect(null,mapDispatchToProps)(SearchHeader);