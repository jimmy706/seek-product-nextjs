import React from "react";
import { Menu, Dropdown, Icon } from "antd";
import Link from "next/link";
import HeaderCart from "./HeaderCart";
import { connect } from "react-redux";
import { LOGOUT_ACTION } from "../../constants/sagas-type";

function LoginHeaderInteract(props) {
  const { first_name, last_name } = props.user;

  const menu = (
    <Menu>
      <Menu.Item>
        <Link href="/profile">
          <a>Your profile</a>
        </Link>
      </Menu.Item>
      <Menu.Item>
        <a href="#" onClick={() => props.dispatchLogoutAction()}>
          Logout
        </a>
      </Menu.Item>
    </Menu>
  );

  return (
    <div className="header__interact--login">
      <Dropdown overlay={menu}>
        <span style={{ cursor: "pointer" }}>
          Hello, {`${first_name} ${last_name}`} <Icon type="down" />
        </span>
      </Dropdown>
      <HeaderCart />
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchLogoutAction: () => dispatch({ type: LOGOUT_ACTION })
  };
};

export default connect(null, mapDispatchToProps)(LoginHeaderInteract);
