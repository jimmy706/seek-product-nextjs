import React from "react";
import { connect } from "react-redux";
import GuestHeaderInteract from "./GuestHeaderInteract";
import LoginHeaderInteract from "./LoginHeaderInteract";

function HeaderInteract(props) {
  return (
    <div className="header__interact">
      {Object.keys(props.userState).length ? (
        <LoginHeaderInteract user={props.userState} />
      ) : (
        <GuestHeaderInteract />
      )}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    userState: state.user
  };
};

export default connect(mapStateToProps, null)(HeaderInteract);
