import React from "react";
import Link from "next/link";

export default function GuestHeaderInteract() {
  return (
    <ul className="header__nav">
      <li className="header__li">
        <Link href="/login">
          <a className="link">Login</a>
        </Link>
      </li>
      <li className="header__li">
        <Link href="/register">
          <a className="link">Register</a>
        </Link>
      </li>
    </ul>
  );
}
