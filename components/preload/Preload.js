import React from "react";
import { css } from "@emotion/core";
import { FadeLoader } from "react-spinners";
import { connect } from "react-redux";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

function Preload({ preload }) {
  return (
    <div className="preload" style={{ display: preload ? "flex" : "none" }}>
      <FadeLoader css={override} size={50} color={"#1d71ab"} loading={true} />
    </div>
  );
}

const mapStateToProps = state => {
  return {
    preload: state.preload
  };
};

export default connect(mapStateToProps, null)(Preload);
