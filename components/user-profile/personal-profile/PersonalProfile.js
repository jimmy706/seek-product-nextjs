import React, {Component} from 'react';
import {Form, Input, Row, Col, Icon, Button} from "antd";
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {UPDATE_PROFILE} from "../../../constants/sagas-type";

class PersonalProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allowSubmit: false
        }
    }

    handleChangeInput = e => {
        if(!this.state.allowSubmit){
            this.setState({allowSubmit: true});
        }
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const {email,avatar,show_phone} = this.props.currentUser;
                const newProfile = {...values, email, avatar, show_phone};
                this.props.dispatchOnUpdateProfile(newProfile);
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const {
            first_name,
            last_name,
            work_at,
            job,
            phone_number,
            message
        } = this.props.currentUser;
        return (
            <Form className='profile-section'
                onSubmit={this.handleSubmit}
            >
                <h3 className='profile-title'>
                    Your personal profile:
                </h3>
                <Row gutter={12}>
                    <Col span={12}>
                        <Form.Item>
                            {getFieldDecorator("first_name", {
                                initialValue: first_name,
                                rules: [
                                    {
                                        required: true,
                                        message: "Enter your first name!"
                                    }
                                ]
                            })(
                                <Input
                                    onChange={this.handleChangeInput}
                                    prefix={
                                        <Icon
                                            type="user"
                                            style={{ color: "rgba(0,0,0,.25)" }}
                                        />
                                    }
                                    name="first_name"
                                    placeholder="First name..."
                                />
                            )}
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item>
                            {getFieldDecorator("last_name", {
                                initialValue: last_name,
                                rules: [
                                    {
                                        required: true,
                                        message: "Enter your last name!"
                                    }
                                ]
                            })(
                                <Input
                                    onChange={this.handleChangeInput}
                                    prefix={
                                        <Icon
                                            type="user"
                                            style={{ color: "rgba(0,0,0,.25)" }}
                                        />
                                    }
                                    name="last_name"
                                    placeholder="Last name..."
                                />
                            )}
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={12}>
                    <Col span={12}>
                        <Form.Item>
                            {getFieldDecorator("work_at",{
                                initialValue: work_at
                            })(
                                <Input
                                    onChange={this.handleChangeInput}
                                    prefix={
                                        <Icon
                                            type="folder-open"
                                            style={{ color: "rgba(0,0,0,.25)" }}
                                        />
                                    }
                                    name="work_at"
                                    placeholder="Work at..."
                                />
                            )}
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item>
                            {getFieldDecorator("job",{
                                initialValue: job
                            })(
                                <Input
                                    onChange={this.handleChangeInput}
                                    prefix={
                                        <Icon
                                            type="form"
                                            style={{ color: "rgba(0,0,0,.25)" }}
                                        />
                                    }
                                    name="job"
                                    placeholder="Your job..."
                                />
                            )}
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item>
                    {getFieldDecorator("phone_number",{
                        initialValue: phone_number
                    })(
                        <Input
                            onChange={this.handleChangeInput}
                            prefix={
                                <Icon type="phone" style={{ color: "rgba(0,0,0,.25)" }} />
                            }
                            type="tel"
                            name="phone_number"
                            placeholder="Enter your phone number..."
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator("message", {
                        initialValue: message
                    })(
                        <Input.TextArea
                            onChange={this.handleChangeInput}
                            name="message"
                            placeholder="Enter your message"
                        />
                    )}
                </Form.Item>
                <div className='form-actions'>
                    <Button htmlType='submit' type='primary' disabled={!this.state.allowSubmit}>Update profile</Button>
                </div>
            </Form>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchOnUpdateProfile: (userProfile) => {
            dispatch({
                type: UPDATE_PROFILE,
                payload: userProfile
            })
        }
    }
};

export default connect(null,mapDispatchToProps)(Form.create({name: 'personal_profile'})(PersonalProfile));

PersonalProfile.propTypes = {
    currentUser: PropTypes.object
};