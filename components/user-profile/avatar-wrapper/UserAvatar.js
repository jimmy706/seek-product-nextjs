import React from "react";
import PropTypes from 'prop-types';

export default function UserAvatar(props){
    const { avatar, onChange} = props;
    return (
        <div className="avatar-wrapper">
            <div className="wall">
                <h1>Personal profile</h1>
            </div>
            <label htmlFor="avatar-input" className="avatar-block">
                <img
                    className="avatar-img"
                    src={avatar}
                    alt="Avatar"
                />
            </label>
            <input type="file" onChange={onChange} id="avatar-input" />
        </div>
    )
}

UserAvatar.propTypes = {
    avatar: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};