import React, {useState} from 'react';
import {Form, Input, Icon, Button} from "antd";
import {connect} from 'react-redux';
import {UPDATE_PASSWORD} from "../../../constants/sagas-type";


function ChangePasswordForm(props){
    const {getFieldDecorator} = props.form;
    const [allowSubmit,setAllowSubmit] = useState(false);

    function handleSubmit(e){
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) {
                props.dispatchOnUpdatePassword(values);
            }
        });
    }

    function handleChangeInput() {
        if(!allowSubmit){
            setAllowSubmit(true);
        }
    }

    return (
        <Form className='profile-section'
              onSubmit={handleSubmit}>
            <h3 className='profile-title'>
                Change password:
            </h3>
            <Form.Item>
                {getFieldDecorator("old_password", {
                    rules: [
                { required: true, message: "Please input your Password!" }
                    ]
                })(
                    <Input
                        onChange={handleChangeInput}
                        name="old_password"
                        placeholder="Enter your old password..."
                        type='password'
                        prefix={
                            <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                    />
                )}
            </Form.Item>
            <Form.Item>
                {getFieldDecorator("new_password", {
                    rules: [
                        { required: true, message: "Please input your Password!" }
                    ]
                })(
                    <Input
                        onChange={handleChangeInput}
                        name="new_password"
                        placeholder="Enter your new password..."
                        type='password'
                        prefix={
                            <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                    />
                )}
            </Form.Item>
            <Form.Item>
                {getFieldDecorator("confirm_password", {
                    rules: [
                        { required: true, message: "Please input your Password!" }
                    ]
                })(
                    <Input
                        onChange={handleChangeInput}
                        name="confirm_password"
                        placeholder="Confirm your new password..."
                        type='password'
                        prefix={
                            <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                        }
                    />
                )}
            </Form.Item>
            <div className='form-actions'>
                <Button htmlType='submit' type='primary' disabled={!allowSubmit}>Update password</Button>
            </div>
        </Form>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchOnUpdatePassword: passwords => dispatch({type: UPDATE_PASSWORD, payload: passwords})
    }
};

export default connect(null,mapDispatchToProps)(Form.create({name: 'change_password_form'})(ChangePasswordForm));