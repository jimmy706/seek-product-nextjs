import React from "react";
import PropTypes from 'prop-types';;
export default function AccountInfo(props){
    const {username,email} = props;
    return (
        <div className='profile-section'>
            <h3 className='profile-title'>
                Account info:
            </h3>
            <table>
                <tbody>
                <tr>
                    <td>
                        <b>Username: </b>
                    </td>
                    <td>{username}</td>
                </tr>
                <tr>
                    <td>
                        <b>Email: </b>
                    </td>
                    <td>{email}</td>
                </tr>
                </tbody>
            </table>
        </div>
    )
}

AccountInfo.propTypes = {
    username: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired
};