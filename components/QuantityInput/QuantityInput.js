import React, { useState } from "react";
import { Input, Icon } from "antd";
import PropTypes from "prop-types";

export default function QuantityInput(props) {
  const [value, setValue] = useState(
    props.defaultValue ? props.defaultValue : 1
  );

  function handleChangeInput(e) {
    const val = parseInt(e.target.value);
    if (!isNaN(val)) {
      setValue(parseInt(val));
      if (props.onChange) {
        props.onChange(val);
      }
    }
  }

  function handlePlus() {
    if (value < 100) {
      if (props.onChange) {
        props.onChange(value + 1);
      }
      setValue(value + 1);
    }
  }

  function handleMinus() {
    if (value > 1) {
      if (props.onChange) {
        props.onChange(value - 1);
      }
      setValue(value - 1);
    }
  }

  return (
    <span className="quantity-input">
      <Input
        type="tel"
        value={value}
        addonBefore={
          <Icon
            onClick={handleMinus}
            type="minus"
            style={{ cursor: "pointer" }}
          />
        }
        addonAfter={
          <Icon
            onClick={handlePlus}
            type="plus"
            style={{ cursor: "pointer" }}
          />
        }
        style={{ display: "inline-block", width: "120px" }}
        onChange={handleChangeInput}
      />
    </span>
  );
}

QuantityInput.propTypes = {
  onChange: PropTypes.func,
  defaultValue: PropTypes.number
};
