import React from "react";
import ProductGalleryLoader from "./product-gallery.loader";
import ListLoader from "./list.loader";

export default function ProductDetailLoader() {
  return (
    <div className="product-main-info product-section">
      <ProductGalleryLoader />
      <ListLoader />
    </div>
  );
}
