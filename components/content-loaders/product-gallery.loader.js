import React from "react";
import ContentLoader from "react-content-loader";

const ProductGalleryLoader = () => (
  <ContentLoader
    height={350}
    width={350}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#ecebeb"
  >
    <rect x="0" y="0" rx="5" ry="5" width="300" height="300" />
    <rect x="0" y="310" rx="0" ry="0" width="50" height="50" />
    <rect x="60" y="310" rx="0" ry="0" width="50" height="50" />
    <rect x="120" y="310" rx="0" ry="0" width="50" height="50" />
    <rect x="180" y="310" rx="0" ry="0" width="50" height="50" />
    <rect x="240" y="310" rx="0" ry="0" width="50" height="50" />
  </ContentLoader>
);

export default ProductGalleryLoader;
