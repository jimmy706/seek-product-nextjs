import React from "react";
import PropTypes from "prop-types";
import getCurrency from "../../utils/get-currency";
import {Icon} from "antd";
import QuantityInput from "../QuantityInput/QuantityInput";
import {REMOVE_ITEM_FROM_CART, UPDATE_CART_ITEM} from "../../constants/sagas-type";
import {connect} from 'react-redux';

function CartItem(props) {
  const { image, name, price, id } = props.item.product;

  function handleChangeQuantity(quantity) {
      props.updateCart(id,quantity);
  }

  function handleRemoveItem() {
      props.removeItemFromCart(id);
  }

  return (
    <div className="cart__item">
      <img
        className="cart__item__image"
        src={image ? image : "https://via.placeholder.com/400x300"}
        alt={name}
      />
      <div className="cart__item__info">
        <h4 className="cart__item__item-name">{name}</h4>
        <p>
          Price:
          <b>{getCurrency(price)}</b>
        </p>
      </div>
      <div className="cart__item__interact">
        <Icon
          type="close"
          className="cancel-item-icon"
          title="Delete this item"
          onClick={handleRemoveItem}
        />
        <QuantityInput
          onChange={handleChangeQuantity}
          defaultValue={props.item.amount}
        />
      </div>
    </div>
  );
}

const mapDispatchToProps = dispatch => {
    return {
        removeItemFromCart: (product) => dispatch({type: REMOVE_ITEM_FROM_CART,product}),
        updateCart: (product,amount) => dispatch({type: UPDATE_CART_ITEM, product, amount})
    }
}

export default connect(null,mapDispatchToProps)(CartItem);

CartItem.propTypes = {
  item: PropTypes.object.isRequired
};
