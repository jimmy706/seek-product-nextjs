import React from "react";
import { Icon } from "antd";
import Link from "next/link";

export default function EmptyCart() {
  return (
    <div className="cart__empty-cart">
      <Icon type="info-circle" className="info-icon" />
      <h1>Your cart is empty now!</h1>
      <Link href="/">
        <a>
          <Icon type="arrow-down" /> <br />
          Back to shopping
        </a>
      </Link>
    </div>
  );
}
