import React from "react";
import Link from "next/link";
import getCurrency from "../../utils/get-currency";
import {connect} from "react-redux";
import {Button} from "antd";

function PaymentBoard(props) {
  const { totalPrice } = props;

  return (
    <div className="cart__payment">
      <div>
        Total price:
        <h2>{getCurrency(totalPrice)}</h2>
      </div>
      <Link href={`/confirm-payment?price=${totalPrice}`}>
        <a>
          <Button block type="primary">
            Pay
          </Button>
        </a>
      </Link>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    totalPrice: state.cart.total
  };
};

export default connect(mapStateToProps, null)(PaymentBoard);
